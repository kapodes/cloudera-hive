#!/bin/bash
# -d flag for work in background or start verbosly
docker build -t cloudera-hive .
docker rm cloudera-hive-container
docker create --name cloudera-hive-container --memory=2g --publish 8888:8888 --publish 10000:10000 -v ~/ClouderaVolume/:/external/ --hostname=quickstart.cloudera --privileged=true -t -i cloudera-hive:latest