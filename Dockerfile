FROM cloudera/quickstart:latest

#remove original db's
RUN rm -rf /var/lib/mysql/hue
RUN rm -rf /var/lib/mysql/mysql
RUN rm -rf /var/lib/mysql/metastore

#copy hdfs config
COPY ./main/src/etc/hadoop/conf.pseudo /etc/hadoop/conf.pseudo

#copy hue config
COPY ./main/src/etc/hue /etc/hue

RUN mkdir /external/
RUN mkdir /external/mysql

RUN mkdir /external/mysql/hue
RUN ln -s /external/mysql/hue /var/lib/mysql/hue

RUN mkdir /external/mysql/mysql
RUN ln -s /external/mysql/mysql /var/lib/mysql/mysql

RUN mkdir /external/mysql/metastore
RUN ln -s /external/mysql/metastore /var/lib/mysql/metastore

#copy scripts for custom running and stopping
COPY ./main/src/docker-start /usr/bin/docker-start
RUN chmod +x /usr/bin/docker-start

COPY ./main/src/docker-stop /usr/bin/docker-stop
RUN chmod +x /usr/bin/docker-stop

#hue port
EXPOSE 8888
#hive port
EXPOSE 10000
#sentry port
#EXPOSE 8038:8038

CMD /usr/bin/docker-start